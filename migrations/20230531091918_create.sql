create table images (
    filepath TEXT PRIMARY KEY not null,
    filename TEXT not null,
    extension TEXT not null,
    width INT not null,
    height INT not null,
    modified INT not null
);

create table tags (
    filepath TEXT not null,
    tag TEXT not null,
    foreign key (filepath) references images(filepath) on delete cascade
);
