async function get_images(pagesize, pagenumber, order) {
    data = {
        'pagesize':   pagesize,
        'pagenumber': pagenumber,
        'order':      order,
    };
    var formBody = [];
    for (var property in data) {
        var encodedKey = encodeURIComponent(property);
        var encodedValue = encodeURIComponent(data[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");
    const response = await fetch('/images?' + formBody, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'Accept':       'application/json'
        },
    });
    const jsonData = await response.json();
    console.log(jsonData);
}

async function update_db() {
    const response = await fetch('/update_db', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'Accept':       'application/json'
        },
    });
    const jsonData = await response.json();
    console.log(jsonData);
}
