use rocket::{
    fs::{FileServer, relative},
    serde::json::{json, Json},
    State,
    http::Status,
    response::stream::ByteStream,
};
pub mod image_cache;
pub mod thumbnail;
use crate::image_cache::{ImageLib, OrderBy};

type Result<T, E = rocket::response::Debug<sqlx::Error>> = std::result::Result<T, E>;

#[macro_use] extern crate rocket;

#[get("/images?<pagesize>&<pagenumber>&<order>")]
async fn images(
    image_lib: &State<image_cache::ImageLib>, 
    pagesize: usize, 
    pagenumber: usize,
    order: OrderBy,
    ) -> Json<Vec<image_cache::Image>> 
{
    let offset = pagesize*pagenumber;
    let images = image_lib.get_images(pagesize as u32, offset as u32, order).await;
    Json(images)
}

#[get("/tags")]
async fn tags(image_lib: &State<image_cache::ImageLib>) -> Json<Vec<String>>{
    let tags = image_lib.get_tags().await;
    Json(tags)
}

#[get("/tags/image?<path>")]
async fn image_tags(image_lib: &State<image_cache::ImageLib>, path: &str) -> Json<Vec<String>>{
    let tags = image_lib.get_image_tags(path.to_string()).await;
    Json(tags)
}

#[get("/update_db")]
async fn update_db(image_lib: &State<image_cache::ImageLib>) -> Result<()> {
    image_lib.update_db().await?;
    Ok(())
}

#[get("/thumb/<path>")]
async fn thumb(path: &str) -> ByteStream![Vec<u8>] {
    ByteStream! {
        yield vec![1,2,4];
    }
}

#[launch]
async fn rocket() -> _ {
    let rocket = rocket::build()
        .mount("/", 
           routes![images, update_db, tags, image_tags, thumb]
        );
    let indexed_image_lib = ImageLib::init().await;
    let image_path = &indexed_image_lib.config.image_path.clone();
    rocket
        .manage(indexed_image_lib)
        .mount("/", FileServer::from(relative!("./static/")))
        .mount("/img", FileServer::from(image_path).rank(30))
}
