extern crate walkdir;
extern crate toml;
extern crate image;
extern crate base16ct;
use rocket::serde::{Serialize, Deserialize};
use std::path::Path;
use walkdir::WalkDir;
use std::{fs, io::Read};
use sqlx::{sqlite::SqlitePool, QueryBuilder, Sqlite};

type Result<T, E = rocket::response::Debug<sqlx::Error>> = std::result::Result<T, E>;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct Config {
    pub image_path: String,
    pub extensions: Vec<String>,
    pub db_url: String,
    pub url_prefix: String,
    pub cache_dir: String,
}

#[derive(Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Image{
    pub file_name: String,
    pub path: String,
    pub url: String,
    pub thumbnail: String,
    pub width: i64,
    pub height: i64,
    pub modified: i64,
}

#[derive(sqlx::FromRow)]
struct DbImage{
    filepath: String,
    filename: String,
    extension: String,
    width: i64,
    height: i64,
    modified: i64,
}

#[derive(sqlx::FromRow)]
struct DbTag{
    tag: String,
}

#[derive(Debug, FromFormField)]
pub enum OrderBy{
    ModifiedAsc,
    ModifiedDesc,
    PathAsc,
    PathDesc,
}

pub struct ImageLib{
    pub config: Config,
    db: SqlitePool,
}


impl ImageLib {
    pub async fn init() -> Self {
        // 1. initalize config file & database
        let mut config_file = fs::File::open("Gallery.toml").expect("Gallery.toml not found");
        let mut config = String::new();
        config_file.read_to_string(&mut config);
        let config: Config = toml::from_str(&config).expect("Config parsing error");

        let db_pool = SqlitePool::connect(&config.db_url).await.expect("Database initaliziation failed");

        // 2. query db
        /*
        let _result = sqlx::query_as!(
            DbImage,
            r#"
            select * from images;
            "#,
            )
            .fetch_all(&db_pool).await.expect("something with the query has gone wrong");
        */
        // 3. (test for missing files)

        ImageLib {
            config: config,
            db: db_pool,
        }
    }
    pub async fn get_images(&self, limit: u32, offset: u32, order_by: OrderBy) -> Vec<Image> {
        let order = match order_by {
            OrderBy::PathAsc => "filepath asc",
            OrderBy::PathDesc => "filepath desc",
            OrderBy::ModifiedAsc => "modified asc",
            OrderBy::ModifiedDesc => "modified desc",
        };
        let mut query: QueryBuilder<Sqlite> = QueryBuilder::new( r#"
            select * from images 
            order by "#);
        query.push(order)
            .push(" limit ").push_bind(limit)
            .push(" offset ").push_bind(offset);
        query.build_query_as::<DbImage>()
            .fetch_all(&self.db).await.expect("something with the query has gone wrong")
            .iter().map(|db_image| db_image.to_image(&self.config.url_prefix)).collect()
    }
    pub async fn get_image_tags(&self, filepath: String) -> Vec<String> {
        sqlx::query_as!(
            DbTag,
            r#"
            select tag
            from images as i join tags as t 
                on i.filepath = t.filepath
            where i.filepath = ?1
            "#,
            filepath
            ).fetch_all(&self.db).await.expect("query failed")
            .iter().map(|tag| tag.to_string()).collect()
    }
    pub async fn get_tags(&self) -> Vec<String> {
        sqlx::query_as!(
            DbTag,
            r#"
            select distinct tag
            from tags
            "#,
            ).fetch_all(&self.db).await.expect("query failed")
            .iter().map(|tag| tag.to_string()).collect()
    }
    pub async fn update_db(&self) -> Result<()> {
        for image in WalkDir::new(&self.config.image_path)
            .into_iter().filter_map(|file| file.ok()) 
        {
            if image.metadata().unwrap().is_file() {
                let image = image.path();
                match image.extension() {
                    Some(extension) if self.config.extensions.iter().any(|ext| ext.as_str() == extension) => {
                        let i = DbImage::from_path(image, &self.config.image_path);
                        let tags: Vec<String> = Path::new(&i.filepath).parent().unwrap()
                            .components().filter_map(|p| match p {
                            std::path::Component::Normal(os_str) => Some(os_str.to_str().unwrap().to_string()),
                            _ => None,
                        }).collect();
                        // here should be the thumbnail creation
                        sqlx::query!(
                            r#"
                            insert into images (filepath, filename, extension, width, height, modified)
                            values (?1, ?2, ?3, ?4, ?5, ?6)
                            on conflict(filepath) do update
                                set width = ?4, height = ?5, modified = ?6;
                            "#,
                            i.filepath, 
                            i.filename, 
                            i.extension, 
                            i.width,
                            i.height,
                            i.modified)
                            .execute(&self.db).await?;
                            
                        if !tags.is_empty() {
                            let mut query: QueryBuilder<Sqlite> = QueryBuilder::new( r#"
                                insert into tags (filepath, tag) 
                            "#,);
                            query.push_values(tags.iter(), |mut b, tag| {
                                b.push_bind(&i.filepath).push_bind(tag);
                            });
                            query.build().execute(&self.db).await?;
                        }
                    }
                    _ => (),
                }
            };
        }
        Ok(())
    }
}

impl DbImage {
    fn from_path(path: &Path, bib_path: &String) -> Self {
        let file_name = path.file_name().unwrap().to_str().unwrap().to_string();
        let file_path = path.to_str().unwrap().to_string();
        let extension = path.extension().unwrap().to_str().unwrap().to_string();
        let rel_path = file_path.clone().replace(bib_path, "");
        let (width, height) = match image::image_dimensions(path) {
            Ok(dimensions) => dimensions,
            Err(_) => (0, 0),
        };
        let modified = std::fs::metadata(path)
            .expect("this file doesn't exist")
            .modified()
            .expect("platform does not support file creation time")
            .duration_since(std::time::SystemTime::UNIX_EPOCH)
            .expect("file creation time is before unix epoch!?")
            .as_millis();
        DbImage {
            filename:  file_name,
            filepath:  rel_path.clone(),
            extension: extension,
            width:     width as i64,
            height:    height as i64,
            modified:  modified as i64,
        }
    }
    fn to_image(&self, url_prefix: &str) -> Image {
        let filepath = &self.filepath;
        let extension = &self.extension;
        let thumbnail = base16ct::lower::encode_string(filepath.as_bytes());
        Image{
            file_name: self.filename.clone(),
            path:      filepath.clone(),
            url:       url_prefix.clone().to_owned() + "/img" + filepath,
            thumbnail: url_prefix.clone().to_owned() + "/thumb/" + &thumbnail + "." + &extension,
            width:     self.width,
            height:    self.height,
            modified:  self.modified,
        }
    }
}

impl DbTag{
    fn to_string(&self) -> String {
        self.tag.to_string()
    }
}
